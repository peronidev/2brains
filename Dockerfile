FROM node:9.11.1-alpine as build-stage
WORKDIR /app
COPY frontend/package*.json ./
RUN npm install
COPY frontend .
RUN npm run build

FROM nginx:1.13.12-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN adduser -D desafio_tecnico
WORKDIR /home/desafio_tecnico
RUN mkdir log
RUN mkdir /var/log/desafio_tecnico
COPY requirements.txt requirements.txt
RUN apk update
RUN apk add gcc g++ make libffi-dev openssl-dev python3 python3-dev supervisor redis
RUN python3 -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn
COPY app app
COPY manage.py config.py boot.sh serviceAccountKey.json ./
COPY supervisord.conf /etc/supervisord.conf
COPY nginx.conf gunicorn.conf redis.conf /etc/supervisor/conf.d/
RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf
RUN chmod +x boot.sh
RUN chown -R desafio_tecnico:desafio_tecnico ./
EXPOSE 80 5000
CMD ["/usr/bin/supervisord"]
