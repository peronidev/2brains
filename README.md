# Desafío técnico 2Brains

Aplicación desarrollada para el desafío técnico para optar al puesto de FullStack Developer en 2Brains.

## Comenzando

Estas instrucciones son para desplegar la aplicación de forma local. Los pasos serán explicados para un entorno en un sistema Unix tales como Linux y Mac, en este caso específico será para un sistema basado en Ubuntu. Algunos de los comandos señalados quizás requieran tener permisos de superusuario (sudo).

### Pre-requisitos

Es necesario contar con git y docker instalados en el sistema. Los comandos para su instalación son:

```
sudo apt-get install git docker-ce
sudo apt install docker.io (Instancia Debian AWS)
```

Una vez instalados, hay que clonar el repositorio.

```
git clone https://bitbucket.org/peronidev/2brains.git
```

## Despliegue

Ingresamos al directorio de la aplicación "_2brains_". Una vez allí, ejecutamos la creación de la imagen docker. Este paso generará la imagen que se ocupará para crear el contenedor. Además, se instala la aplicación y todas sus dependencias:

```
docker build -t 2brains:latest .
```

Este paso puede tomar varios minutos dependiendo de los recursos de la máquina o servidor donde se esté trabajando. Una vez listo este paso, ejecutamos el contenedor:

```
docker run -it -p 8080:80 -p 5000:5000 --rm --name 2brains-desafio-tecnico 2brains:latest
```

La opción _--rm_ es para eliminar el contenedor una vez se detenga su ejecución.

Con la aplicación ejecutándose, abrimos un navegador e ingresamos a la dirección _localhost:8080_. Aparecerá una ventana de login, las credenciales son:

```
Usuario: demo@demo.cl
Password: d3m0.d3m0
```

Para revisar los logs de la aplicación es necesario ingresar al contenedor. Para ello abrimos una nueva terminal y escribimos:

```
sudo docker exec -it 2brains-desafio-tecnico sh
```

Los logs de la aplicación se encuentran en /var/log/desafio_tecnico/backend.err.log.

Dentro de la aplicación, se pueden observar los requerimientos solicitados para este desafío técnico.
Una vez finalizada la revisión, cerramos sesión en el botón superior de Logout. Posteriormente, terminamos la ejecución de la aplicación deteniendo el contenedor docker abierto en la terminal.

## Construido con

#### Backend

* [Flask 1.1.1](https://flask.palletsprojects.com/) - Framework web python
* [Python 3](https://www.python.org/download/releases/3.0/) - Lenguaje de programación
* [Redis](https://redis.io/) - Almacén de estructura de datos en memoria
* [Docker](https://www.docker.com/) - Tecnología para la creación de contenedores para el despliegue de aplicaciones

#### Frontend

* [Vue 2.5.2](https://vuejs.org/) - Framework JS Progresivo
* [Vue-router 3.0.1](https://router.vuejs.org/) - Enrutador para Vue
* [Vuex 3.1.2](https://vuex.vuejs.org/) - Manejador de almacén de datos para el estado de aplicaciones
* [Bootstrap 4.4.1](https://getbootstrap.com/) - Framework frontend para el desarrollo de sitios responsivos
* [Bootstrap-vue 2.1.0](https://bootstrap-vue.js.org/) - Integración de Bootstrap para Vue

## Autor

* **Juan Carlos Ramos Peroni** - Desarrollo - [peronidev](https://bitbucket.org/peronidev)
