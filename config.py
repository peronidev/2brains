# -*- coding: utf-8 -*-
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SRC_ROOT = os.path.dirname(os.path.abspath(__file__))  # application_top
    APP_ROOT = os.path.join(SRC_ROOT, 'app')
    APP_STATIC = os.path.join(APP_ROOT, 'static')
    LOG_ROOT = os.environ.get('LOG_ROOT') or os.path.join(SRC_ROOT, 'log')

    SECRET_KEY = os.environ.get('SECRET_KEY', 'j!P0is%7YujRf8K23w1')
    JSONIFY_PRETTYPRINT_REGULAR = False

    CACHE_TYPE = os.environ.get('CACHE_TYPE', 'redis')
    CACHE_REDIS_URL = os.environ.get('CACHE_REDIS_URL', 'redis://localhost:6379/0')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = os.environ.get('DEBUG') or True
    TESTING = os.environ.get('TESTING') or False


class TestingConfig(Config):
    DEBUG = os.environ.get('DEBUG') or False
    TESTING = os.environ.get('TESTING') or True


class ProductionConfig(Config):
    DEBUG = os.environ.get('DEBUG') or False
    TESTING = os.environ.get('DEBUG') or False


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': ProductionConfig
}
