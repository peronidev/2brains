import {AUTH_REQUEST, AUTH_SUCCESS, AUTH_ERROR, AUTH_LOGOUT} from '../actions/auth'
import Axios from 'axios'

const state = {
  token: localStorage.getItem('user-token') || '',
  status: '',
  hasLoadedOnce: false
}

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
}

const actions = {
  [AUTH_REQUEST]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST)
      Axios({url: document.location.origin.split(':8080')[0] + ':5000/auth/login', method: 'POST', data: user})
        .then(resp => {
          if (resp.data.status === 200) {
            localStorage.setItem('user-token', resp.data.id_token)
            Axios.defaults.headers.common['Authorization'] = resp.data.id_token
            console.log(Axios.defaults.headers.common)
            commit(AUTH_SUCCESS, resp.data)
            resolve(resp.data)
          } else {
            commit(AUTH_ERROR, resp.data)
            localStorage.removeItem('user-token')
            reject(resp.data)
          }
        })
        .catch(err => {
          commit(AUTH_ERROR, err)
          localStorage.removeItem('user-token')
          reject(err)
        })
    })
  },
  [AUTH_LOGOUT]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT)
      Axios({url: document.location.origin.split(':8080')[0] + ':5000/auth/logout', method: 'GET'})
        .then(resp => {
          resolve(resp.data)
        })
        .catch(err => {
          reject(err)
        })
      localStorage.removeItem('user-token')
      resolve()
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [AUTH_SUCCESS]: (state, resp) => {
    state.status = 'success'
    state.token = resp.id_token
    state.hasLoadedOnce = true
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = ''
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
