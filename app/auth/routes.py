# -*- coding: utf-8 -*-
from flask import jsonify, request, current_app
from app.auth import auth
import pyrebase
from requests.exceptions import HTTPError
import json
from firebase_admin import auth as firebase_auth

config = {
  "apiKey": "AIzaSyBIk-3NMTHhUVhv6iobjIDHvy0rKJixIlk",
  "authDomain": "brains-f1c05.firebaseapp.com",
  "databaseURL": "https://brains-f1c05.firebaseio.com",
  "projectId": "brains-f1c05",
  "storageBucket": "brains-f1c05.appspot.com",
  "messagingSenderId": "409989976866",
  "appId": "1:409989976866:web:84c1f6ae00f9063f200b7b"
}

FIREBASE = pyrebase.initialize_app(config)


@auth.route('/login', methods=['POST'])
def login():
    """Metodo login"""
    current_app.logger.info("Login")
    response = {
        'id_token': '',
        'status': 200,
        'msg': 'OK'
    }
    try:
        # Realizar autenticacion Firebase
        _request = request.get_json()
        user = FIREBASE.auth().sign_in_with_email_and_password(_request.get('email', ""), _request.get('password', ""))
        response['id_token'] = user.get('idToken')
    except HTTPError as http_error:
        current_app.logger.error(http_error)
        msg_error = json.loads(http_error.errno.response.text)
        status = msg_error.get('error', {}).get('code', 400)
        if msg_error.get('error', {}).get('message', '') == 'INVALID_EMAIL':
            msg = u'Correo inválido'
        elif msg_error.get('error', {}).get('message', '') == 'INVALID_PASSWORD':
            msg = u'Contraseña inválida'
        elif msg_error.get('error', {}).get('message', '') == 'EMAIL_NOT_FOUND':
            msg = u'No existe correo'
        elif msg_error.get('error', {}).get('message', '') == 'TOO_MANY_ATTEMPTS_TRY_LATER':
            msg = u'Demasiados intentos, intente más tarde'
        else:
            msg = 'Error, vuelva a intentarlo'
        response['status'] = status
        response['msg'] = msg
    except Exception as e:
        current_app.logger.exception(e)
        response['status'] = 500
        response['msg'] = 'Error, vuelva a intentarlo'

    return jsonify(response)


@auth.route('/logout', methods=['GET'])
def logout():
    """Metodo logout"""
    response = {
        'status': 200,
        'msg': 'OK'
    }
    current_app.logger.info('Logout')
    try:
        # Revocar token
        id_token = request.headers.get('Authorization', None)
        verify_token = firebase_auth.verify_id_token(id_token, check_revoked=True)
        firebase_auth.revoke_refresh_tokens(verify_token.get('uid', ''))
        current_app.logger.info('Logout successful')
    except Exception as e:
        current_app.logger.exception("Error logout")
        current_app.logger.exception(e)
        response['status'] = 500
        response['msg'] = 'NOK'
    return jsonify(response)
