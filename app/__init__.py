# -*- coding: utf-8 -*-
from flask import Flask, redirect, url_for
from flask_caching import Cache
from flask_cors import CORS
import firebase_admin


cache = Cache()
cors = CORS()


def create_app(config_name):
    """Metodo para crear la aplicacion siguiendo el patron Factory Method"""
    from config import config
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    import logging
    from logging.handlers import RotatingFileHandler
    import os
    handler = RotatingFileHandler(
        os.path.join(app.config['LOG_ROOT'], '{0}.log'.format(__name__)),
        maxBytes=10000,
        backupCount=1)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s] - Desafio - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    cors.init_app(app, resources={r'/*': {'origins': '*'}})
    cache.init_app(app, config={'CACHE_TYPE': app.config['CACHE_TYPE'],
                                'CACHE_REDIS_URL': app.config['CACHE_REDIS_URL']})

    cred = firebase_admin.credentials.Certificate(os.path.join(app.config['SRC_ROOT'], 'serviceAccountKey.json'))
    firebase_admin.initialize_app(cred)

    from app.auth import auth as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.api import api as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    @app.route('/')
    def index():
        return redirect(url_for('auth.login'))

    @app.errorhandler(404)
    def uri_not_found(error):
        app.logger.error(error)
        return redirect(url_for('index'))

    return app
