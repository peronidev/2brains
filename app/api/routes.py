# -*- coding: utf-8 -*-
import requests
import random
import time
from flask import jsonify, request, current_app, url_for
from app.api import api
from app import cache
from firebase_admin import auth

URI = 'https://randomuser.me/api/?results={0}&page={1}'


@cache.memoize(timeout=60)
def resilient_request(page):
    """Metodo para realizar request persistente, simulando 10% de error. Se define un maximo de consultas muy grande"""
    results_qty = 12
    url = URI.format(results_qty, page)
    base_sleep_time = 1
    max_retries = 1000000
    attempt = 1
    percent = 10

    # Realizar persistentemente la consulta hasta obtener codigo 200,
    # en cualquier caso duerme la consulta siguiendo una progresion geometrica
    while True:
        try:
            response = requests.get(url)
            rand = random.randint(1, 100)
            print(rand)

            if response.status_code == 200 and rand >= percent:
                collaborators_lst = response.json().get('results', [])
                return collaborators_lst
            else:
                current_app.logger.error("10% de falla. Repetir consulta")
                time.sleep(pow(2, attempt) * base_sleep_time)
                attempt += 1
                if attempt > max_retries:
                    return []
        except (ConnectionError, TimeoutError) as e:
            current_app.logger.exception(e)
            time.sleep(pow(2, attempt) * base_sleep_time)
            attempt += 1
            if attempt > max_retries:
                return []
        except Exception as e:
            current_app.logger.exception(e)


@api.route('/collaborators', methods=['GET'])
def collaborators():
    """Metodo para obtener los colaboradores desde randomuser.me"""
    id_token = request.headers.get('Authorization', None)
    page = request.args.get('page', 1, type=int)
    total_pages = 10
    response = {
        'status': 200,
        'msg': 'OK'
    }
    try:
        # Validar token y posteriormente realizar request
        auth.verify_id_token(id_token, check_revoked=True)
        response['collaborators'] = resilient_request(page)
        response['_links'] = {
            "self": "{0}?page={1}".format(url_for('api.collaborators'), page),
            "next": "{0}?page={1}".format(url_for('api.collaborators'), page + 1) if page + 1 <= total_pages else None,
            "prev": "{0}?page={1}".format(url_for('api.collaborators'), page - 1) if page - 1 > 0 else None
        }
        response['_meta'] = {
            "page": page,
            "total_pages": total_pages
        }
    except (auth.ExpiredIdTokenError, auth.InvalidIdTokenError, auth.RevokedIdTokenError) as id_token_error:
        current_app.logger.error(id_token_error)
        response['status'] = 403
        response['msg'] = 'Forbidden'
    except ValueError as value_error:
        current_app.logger.error(value_error)
        response['status'] = 500
        response['msg'] = 'Illegal ID token'
    except Exception as e:
        current_app.logger.exception(e)
        response['status'] = 500
        response['msg'] = 'Error'

    return jsonify(response), response['status']
